<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">About</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="#">About</a></li>
                                <li class="active text-gray-silver">Sahre and Care Initiative</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: About -->
        <section>
        <div class="container">
            <div class="section-content">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Share n Care </span> Initiative</h2>
                    <h4 class="text-theme-color-blue">To Society, With Love - Reach out to Recycle</h4>
                    <p>TMAD is going to organize periodic collection of used items in good shape, form and quality and facilitate the donation of those items where there is genuine need. Any item which we do not use and want to give, share those details.</p>

                    <p>We also need volunteers to collect items and to deliver to the needy. So if you have time to spare or vehicle to drive, please be part of this initiative. You need not be a TMAD member to take up this activity, though we love to have you as our member.</p>

                    <p>If you are an individual or an organization in need of any items, please fill the form. We will route to you when the related items are available.</p>
                    <p>We are very glad that you chose to be part of/associate with our Share n Care Initiative.  Please fill the form with details.</p>
                </div>
                <div class="col-md-6">
                    <div class="video-popup">                
                        <a>
                        <img alt="" src="images/about/7.jpg" class="img-responsive img-fullwidth">
                        </a>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div> 
            <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
        </div>
        </section>

           
           

          

         

           

        </div>

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>