 <!-- Header -->
        <header id="header" class="header">
            <!-- header top -->
            <div class="header-top bg-theme-color-sky sm-text-center p-0">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="widget no-border m-0">
                                <ul class="list-inline font-13 sm-text-center mt-5">
                                    <li>
                                        <a class="text-white" href="#">FAQ</a>
                                    </li>
                                    <li class="text-white">|</li>
                                    <li>
                                        <a class="text-white" href="#">Help Desk</a>
                                    </li>


                                </ul>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="widget no-border m-0 mr-15 pull-right flip sm-pull-none sm-text-center">
                                <ul
                                    class="styled-icons icon-circled icon-sm pull-right flip sm-pull-none sm-text-center mt-sm-15">
                                    <li><a href="#"><i class="fa fa-facebook text-white"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter text-white"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus text-white"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram text-white"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin text-white"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ header top -->

            <!-- header middle -->
            <div class="header-middle p-0 bg-lightest xs-text-center">
                <div class="container pt-0 pb-0">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-5">
                            <div class="widget no-border m-0">
                                <a class="menuzord-brand pull-left flip xs-pull-center mb-15"
                                    href="javascript:void(0)"><img src="images/logo-wide.png" alt=""></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
                                <ul class="list-inline">
                                    <li><i
                                            class="fa fa-phone-square text-theme-color-sky font-36 mt-5 sm-display-block"></i>
                                    </li>
                                    <li>
                                        <a href="#" class="font-12 text-black text-uppercase">Call us today!</a>
                                        <h5 class="font-14 text-theme-color-blue m-0"> +91 944 000 1050</h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-3">
                            <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
                                <ul class="list-inline">
                                    <li><i class="fa fa-clock-o text-theme-color-red font-36 mt-5 sm-display-block"></i>
                                    </li>
                                    <li>
                                        <a href="#" class="font-12 text-black text-uppercase">We are open!</a>
                                        <h5 class="font-13 text-theme-color-blue m-0"> Mon-Fri 8:00-16:00</h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header middle -->

            <!-- header main -->
            <div class="header-nav">
                <div
                    class="header-nav-wrapper navbar-scrolltofixed bg-theme-color-red border-bottom-theme-color-sky-2px">
                    <div class="container">
                        <nav id="menuzord" class="menuzord bg-theme-colored pull-left flip menuzord-responsive">
                            <ul class="menuzord-menu">
                                <li class="active"><a href="index.php">Home</a></li>
                                <li><a href="#">About us</a>
                                    <ul class="dropdown">
                                        <li><a href="about-share-care.php">Share and Care Initiative</a></li>
                                        <li><a href="weneedur-help.php">We Need Your help</a></li>
                                        <li><a href="joinus.php">Join us</a></li>
                                        <li><a href="our-inspiration.php">Our Inspiration</a></li>
                                        <li><a href="faq.php">FAQ</a></li>
                                        <li><a href="news-letter.php">News Letter</a></li>
                                        <li><a href="reportan-issue.php">Report an Issue</a></li>
                                    </ul>
                                </li>
                                <li><a href="events.php">Events</a></li>
                                <li><a href="#">Projects</a>
                                    <ul class="dropdown">
                                        <li><a href="projects-medical.php">Medical</a></li>
                                        <li><a href="projects-educational.php">Education</a></li>
                                        <li><a href="projects-environmental.php">Environmental</a></li>
                                        <li><a href="projects-awarenesscamps.php">Awareness Camps</a></li>
                                        <li><a href="projects-others.php">Other Projects</a></li>
                                    </ul>
                                </li>
                                <li><a href="financials.php">Financials</a></li>

                                <li><a href="javascript:void(0)">Gallery</a>
                                    <ul class="dropdown">
                                        <li><a href="gallery-photos.php">Photos</a></li>
                                        <li><a href="javascript:void(0)">Videos</a></li>
                                    </ul>
                                </li>
                                <li><a href="javascript:void(0)">Chapters</a>
                                    <ul class="dropdown">
                                        <li><a href="chapters-usa.php">USA</a></li>
                                        <li><a href="chapters-hyd.php">Hyderabad</a></li>
                                        <li><a href="chapters-bengaluru.php">Bengaluru</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.php">Contact</a></li>
                                <li><a href="sitemap.php">sitemap</a></li>
                                <li><a href="newsroom.php">New Room</a></li>
                            </ul>
                            <ul class="pull-right flip hidden-sm hidden-xs">
                                <li>
                                    <!-- Modal: Book Now Starts -->
                                    <a class="btn btn-colored btn-flat bg-theme-color-sky text-white font-14 bs-modal-ajax-load mt-0 p-25 pr-15 pl-15"
                                        href="donate.php">Donate Now</a>
                                    <!-- Modal: Book Now End -->
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <!--/ bottom nav -->
        </header>