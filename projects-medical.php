<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">Medical</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li>                                
                                <li><a href="#">Projects</a></li>
                                <li class="active text-gray-silver">Medical</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: About -->
        <section>
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join</span>Us</h2> -->
                        <h4>TMAD undertakes below activities as part of medical</h4>

                        <ul class="table-list">
                            <li><i class="fa fa-check"></i> Financial support to those who cannot afford the medical expenditure.</li>
                            <li><i class="fa fa-check"></i> Organize awareness campaigns.</li>
                            <li><i class="fa fa-check"></i> Organize medical & blood donation camps.</li>
                        </ul>  

                        <h4>Medical Activities undertaken by TMAD from 2005 to till date</h4>

                        <h3>April 2018 - March 2019</h3>

                        <table class="table">

                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Modified Date</th>
                                    <th>Hits</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Vijay Santhosh, 5 yrs, Speech Therapy, Gachibowli, Hyderabad, TS</td>
                                    <td>19 October 2019</td>
                                    <td>158</td>
                                </tr>
                                <tr>
                                    <td>2019 New Year Celebration: Medicines distribution @ Kumidini Hospice, Hyderabad, TG</td>
                                    <td>06 October 2019</td>
                                    <td>294</td>
                                </tr>
                                <tr>
                                    <td>Medical Help - Bhavani</td>
                                    <td>06 October 2019</td>
                                    <td>147</td>
                                </tr>
                                <tr>
                                    <td>Baby Alekhya, 7 Yrs, Brain Surgery, Nizamabad, TG</td>
                                    <td>07 July 2019</td>
                                    <td>709</td>
                                </tr>
                                <tr>
                                    <td>Phanindra, 15 Yrs, Neuro treatment, Hyderabad, TG</td>
                                    <td>19 August 2018</td>
                                    <td>738</td>
                                </tr>
                                <tr>
                                    <td>Lakshmi, 33 Yrs, Leg Injury, Sanjaynagar, Bangalore, KA</td>
                                    <td>05 August 2018</td>
                                    <td>644</td>
                                </tr>
                                <tr>
                                    <td>Gopika Kumar, 4.5 Yrs, Heart Operation, Tiruchi, TN</td>
                                    <td>31 July 2018</td>
                                    <td>1021</td>
                                </tr>
                                <tr>
                                    <td>Babu, 30 Yrs, Kidney failure, Martur, Prakasam, AP</td>
                                    <td>07 July 2018</td>
                                    <td>1435</td>
                                </tr>
                                <tr>
                                    <td>Diabetes Outreach Program</td>
                                    <td>05 July 2018</td>
                                    <td>708</td>
                                </tr>
                                <tr>
                                    <td>Ramakrishna, 33 Yrs, Kidney failure, Kadiri, Anantapur, AP</td>
                                    <td>26 June 2018</td>
                                    <td>1440</td>
                                </tr>
                            </tbody>
                        </table>

                   
                       

                       
                       
                       

                       
                   
                    </div>                
                </div>
            </div>
        </div>
        <div> 
            <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
        </div>
        </section>
           

        </div>
        <!--/ ends main content -->

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>