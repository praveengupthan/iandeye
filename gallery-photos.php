<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">Photos</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li>                                
                                <li><a href="#">Gallery</a></li>
                                <li class="active text-gray-silver">Photos</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: About -->
        <section>
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join</span>Us</h2> -->

                         <!-- Section: Our Gallery -->
            <section>
                <div class="container pb-70">
                    <div class="section-title text-center">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <h2 class="line-bottom-center mt-0">Our <span
                                        class="text-theme-color-red">Gallery</span></h2>
                                <div class="title-flaticon">
                                    <i class="flaticon-charity-alms"></i>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="section-content">
                        <div class="row mb-30">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/gallery/1.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade red"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/gallery/1.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/gallery/2.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade yellow"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/gallery/2.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/gallery/3.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade green"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/gallery/3.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-30">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/gallery/4.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade green"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/gallery/4.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/gallery/5.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade blue"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/gallery5.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/gallery/6.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade yellow"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/gallery/6.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


                    </div>                
                </div>
            </div>
        </div>
        <div> 
            <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
        </div>
        </section>
           

        </div>
        <!--/ ends main content -->

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>