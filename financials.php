<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">Financials</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li>                                
                                <!-- <li><a href="#">Projects</a></li> -->
                                <li class="active text-gray-silver">Financials</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: About -->
        <section>
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join</span>Us</h2> -->

                        <table class="table">

                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Modified Date</th>
                                    <th>Hits</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><a href="javascript:void(0)">April 2018 - March 2019 Financials</a></td>
                                    <td>09 November 2019</td>
                                    <td>75</td>
                                </tr>

                                <tr>
                                    <td><a href="javascript:void(0)">April 2017 - March 2018 Financials</a></td>
                                    <td>23 June 2018</td>
                                    <td>500</td>
                                </tr>

                                <tr>
                                    <td><a href="javascript:void(0)">April 2016 - March 2017 Financials</a></td>
                                    <td>23 June 2018</td>
                                    <td>1373</td>
                                </tr>

                                <tr>
                                    <td><a href="javascript:void(0)">April 2015 - March 2016 Financials</a></td>
                                    <td>19 May 2017</td>
                                    <td>13</td>
                                </tr>

                                <tr>
                                    <td><a href="javascript:void(0)">April 2014 - March 2015 Financials</a></td>
                                    <td>25 June 2016</td>
                                    <td>2008</td>
                                </tr>

                                <tr>
                                    <td><a href="javascript:void(0)">April 2013 - March 2014 Financials</a></td>
                                    <td>25 June 2016</td>
                                    <td>2108</td>
                                </tr>

                                <tr>
                                    <td><a href="javascript:void(0)">April 2012 - March 2013 Financials</a></td>
                                    <td>25 June 2016</td>
                                    <td>4558</td>
                                </tr>

                                <tr>
                                    <td><a href="javascript:void(0)">April 2011 - March 2012 Financials</a></td>
                                    <td>25 June 2016</td>
                                    <td>3816</td>
                                </tr>

                                <tr>
                                    <td><a href="javascript:void(0)">April 2010 - March 2011 Financials</a></td>
                                    <td>25 June 2016</td>
                                    <td>3682</td>
                                </tr>

                                <tr>
                                    <td><a href="javascript:void(0)">April 2009 - March 2010 Financials</a></td>
                                    <td>25 June 2016</td>
                                    <td>3605</td>
                                </tr>
                                
                            </tbody>
                        </table>

                   
                       

                       
                       
                       

                       
                   
                    </div>                
                </div>
            </div>
        </div>
        <div> 
            <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
        </div>
        </section>
           

        </div>
        <!--/ ends main content -->

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>