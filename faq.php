<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">Faq</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="#">About</a></li>
                                <li class="active text-gray-silver">Faq</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: About -->
        <section>
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12 pricing-table">
                        <!-- <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join</span>Us</h2> -->

                        <div class="pb-3">
                            <h5>What is TMAD group?</h5>
                            <p>We are a non government organization based in India (Hyderabad, Bangalore, Chennai and Pune) and US, established on 28 th Feb 2005 and registered on 22 nd Sep 2007 in Hyderabad to provide humanitarian service to the poor and needy in the areas of education, health care, self employment, shelter for the aged, destitute and orphan etc. with collaboration of different like minded groups. The organization has 120 volunteers who are very passionate to make some positive difference in the society. </p>
                        </div>

                        <div class="pb-3">
                            <h5>How do we generate funds?</h5>
                            <p> Monthly contributions from the members</p>
                            <p>Project based contributions (kind hearted donors/NGOs)</p>
                        </div>

                        <div class="pb-3">
                            <h5>What are the cases that we normally prefer?</h5>
                            <p> Being genuine is the only criteria</p>
                            <p>Helping those whom no one else reaches</p>
                            <p>Expect hard working people but not idle ones or parasites</p>
                        </div>

                        <div class="pb-3">
                            <h5>How do we assure of the best use of money?</h5>
                            <p> Give the necessities like medicines, books etc. instead of giving money directly to the needy</p> 
                        </div>

                        <div class="pb-3">
                            <h5>How do we collaborate with other groups?</h5>
                            <p>Based on the common ideology, guidelines, working principles</p> 
                            <p>Visit to Govt. Schools, NGOs (Trusts, Orphanages, Old Age Homes etc.) to find their activities and needs</p>
                        </div>

                        <div class="pb-3">
                            <h5>How do we go about inter/intra communication?</h5>
                            <p>Through group mails and Web portal</p> 
                            <p>Monthly meetings on first Sunday of every month in each chapter</p>
                        </div>

                        <div class="pb-3">
                            <h5>What are our Activities?</h5>
                            <p>A student who needs to pay college fee</p> 
                            <p>A patient who wants money for operation</p>
                            <p>A beggar who needs his survival needs</p>
                            <p>A patient who needs blood</p>
                            <p>A student who needs career guidance</p>
                            <p>A school which needs Educational support to teach their students and amenities for its development</p>
                            <p>Awareness on Right to Information(RTI) Act</p>
                            <p>Awareness on Environmental Issues</p>
                            <p>We work on anything and everything to make a difference</p>
                        </div>

                        <div class="pb-3">
                            <h5>What are the few projects taken so far?</h5>
                            <p>Medical</p> 
                            <p>Education</p>
                            <p>Plantation Drive & Drawing Competition at Govt Kannada Primary School, Vimanapura, Bangalore</p>
                            <p>Celebrate Ganesh Chaturthi in eco friendly way</p>
                            <p>Blood Donation Camp in Bangalore (For Every 4 Months in a Calendar Year)</p>
                            <p>Blanket Distribution Project</p>
                        </div>

                        <div class="pb-3">
                            <h5>How many got benefited/project undertaken till now?</h5>
                            <p>56 Students benefited</p> 
                            <p>73 Patients benefited</p>
                            <p>85 Projects undertaken</p>
                            <p>98 Eye Donors registered</p>
                            <p>130 Monkey caps Distributed</p>
                            <p>174 Hygiene kits donated</p>
                            <p>204 Units of blood donated</p>
                            <p>515 Blankets Distributed</p>
                            <p>603 school kits Distributed</p>
                        </div>

                        <div class="pb-3">
                            <h5>How can you join TMAD?</h5>
                            <p>Please visit the below link.</p> 
                            <p><a href="javascript:void(0)">Click here</a></p>                           
                        </div>

                        <div class="pb-3">
                            <h5>How can you help the needy?</h5>
                            <p>You can assist the patient while consulting Doctor, guiding the student for his/her career, collecting NGOs information through Internet, monetary help etc..</p> 
                        </div>

                        <div class="pb-3">
                            <h5>TMAD Bank A/c Details:</h5>
                            <table>
                            <tr>
                                <td>Account Name: </td>
                                <td>TO MAKE A DIFFERENCE (TMAD)</td>
                            </tr>
                            <tr>
                                <td>Account Number:</td>
                                <td>000801210382</td>
                            </tr>
                            <tr>
                                <td>Account Type:</td>
                                <td>Current</td>
                            </tr>
                            <tr>
                                <td>Branch:</td>
                                <td>CIBD, Hyderabad</td>
                            </tr>
                            <tr>
                                <td>NEFT/IFSC/RTGS Code:</td>
                                <td>ICIC0000008</td>
                            </tr>                            
                        </table>
                        </div>
                       
                       

                       
                   
                    </div>                
                </div>
            </div>
        </div>
        <div> 
            <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
        </div>
        </section>
           

        </div>
        <!--/ ends main content -->

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>