 <!-- Footer -->
        <footer id="footer" class="footer divider layer-overlay overlay-dark-9" data-bg-img="images/bg/bg8.jpg">
            <div class="container">

                <div class="row mt-30">
                    <div class="col-md-2">
                        <div class="widget dark">
                            <h5 class="widget-title mb-10">Call Us Now</h5>
                            <div class="text-gray">
                                +91 944 000 1050
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="widget dark">
                            <h5 class="widget-title mb-10">Connect With Us</h5>
                            <ul class="styled-icons icon-bordered icon-sm">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-5 col-md-offset-2">
                        <div class="widget dark">
                            <h5 class="widget-title mb-10">Subscribe Us</h5>
                            <!-- Mailchimp Subscription Form Starts Here -->
                            <form id="mailchimp-subscription-form-footer" class="newsletter-form">
                                <div class="input-group">
                                    <input type="email" value="" name="EMAIL" placeholder="Your Email"
                                        class="form-control input-lg font-16" data-height="45px" id="mce-EMAIL-footer">
                                    <span class="input-group-btn">
                                        <button data-height="45px"
                                            class="btn bg-theme-color-sky text-white btn-xs m-0 font-14"
                                            type="submit">Subscribe</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom bg-black-333">
                <div class="container pt-20 pb-20">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="font-11 text-black-777 m-0">Copyright &copy;2017 iandEye. All Rights Reserved
                            </p>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="widget no-border m-0">
                                <ul class="list-inline sm-text-center mt-5 font-12">
                                    <li>
                                        <a href="#">FAQ</a>
                                    </li>
                                    <li>|</li>
                                    <li>
                                        <a href="#">Help Desk</a>
                                    </li>
                                    <li>|</li>
                                    <li>
                                        <a href="#">Support</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>