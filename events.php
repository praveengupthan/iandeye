<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">Events</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li>                                
                                <li class="active text-gray-silver">Events</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: About -->
        <section>
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12 pricing-table">
                        <!-- <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join</span>Us</h2> -->

                        <section>
                            <div class="container pb-30">
                                <div class="section-content">
                                <div class="row">
                                    <div class="col-sm-4 col-md-4 col-lg-4">
                                    <div class="schedule-box maxwidth500 mb-30" data-bg-img="images/pattern/p6.png">
                                        <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="images/events/1.jpg">
                                        </div>
                                        <div class="schedule-details clearfix p-15 pt-10">
                                        <div class="text-center pull-left flip bg-theme-color-sky p-10 pt-5 pb-5 mr-10">
                                            <ul>
                                            <li class="font-19 text-white font-weight-600 border-bottom ">28</li>
                                            <li class="font-12 text-white text-uppercase">Feb</li>
                                            </ul>
                                        </div>
                                        <h4 class="title mt-0"><a href="#">Business Conferences 2017</a></h4>
                                        <ul class="list-inline font-11 text-black">
                                            <li><i class="fa fa-calendar mr-5"></i> Sundat at 9:30PM</li>
                                            <li><i class="fa fa-map-marker mr-5"></i> 89 Newyork City.</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <p class="mt-10">Lorem ipsum dolor sit amet elit. Cum veritatis sequi nulla nihil, dolor voluptatum nemo adipisci eligendi! Sed nisi perferendis, totam harum dicta.</p>
                                        <div class="mt-10">
                                            <a href="#" class="btn btn-default btn-theme-color-sky mt-10 font-16 btn-sm" href="page-donate.html">read more</a>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-4">
                                    <div class="schedule-box maxwidth500 mb-30" data-bg-img="images/pattern/p6.png">
                                        <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="images/events/1.jpg">
                                        </div>
                                        <div class="schedule-details clearfix p-15 pt-10">
                                        <div class="text-center pull-left flip bg-theme-color-sky p-10 pt-5 pb-5 mr-10">
                                            <ul>
                                            <li class="font-19 text-white font-weight-600 border-bottom ">28</li>
                                            <li class="font-12 text-white text-uppercase">Feb</li>
                                            </ul>
                                        </div>
                                        <h4 class="title mt-0"><a href="#">Business Conferences 2017</a></h4>
                                        <ul class="list-inline font-11 text-black">
                                            <li><i class="fa fa-calendar mr-5"></i> Sundat at 9:30PM</li>
                                            <li><i class="fa fa-map-marker mr-5"></i> 89 Newyork City.</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <p class="mt-10">Lorem ipsum dolor sit amet elit. Cum veritatis sequi nulla nihil, dolor voluptatum nemo adipisci eligendi! Sed nisi perferendis, totam harum dicta.</p>
                                        <div class="mt-10">
                                            <a href="#" class="btn btn-default btn-theme-color-sky mt-10 font-16 btn-sm" href="page-donate.html">read more</a>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-4">
                                    <div class="schedule-box maxwidth500 mb-30" data-bg-img="images/pattern/p6.png">
                                        <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="images/events/1.jpg">
                                        </div>
                                        <div class="schedule-details clearfix p-15 pt-10">
                                        <div class="text-center pull-left flip bg-theme-color-sky p-10 pt-5 pb-5 mr-10">
                                            <ul>
                                            <li class="font-19 text-white font-weight-600 border-bottom ">28</li>
                                            <li class="font-12 text-white text-uppercase">Feb</li>
                                            </ul>
                                        </div>
                                        <h4 class="title mt-0"><a href="#">Business Conferences 2017</a></h4>
                                        <ul class="list-inline font-11 text-black">
                                            <li><i class="fa fa-calendar mr-5"></i> Sundat at 9:30PM</li>
                                            <li><i class="fa fa-map-marker mr-5"></i> 89 Newyork City.</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <p class="mt-10">Lorem ipsum dolor sit amet elit. Cum veritatis sequi nulla nihil, dolor voluptatum nemo adipisci eligendi! Sed nisi perferendis, totam harum dicta.</p>
                                        <div class="mt-10">
                                            <a href="#" class="btn btn-default btn-theme-color-sky mt-10 font-16 btn-sm" href="page-donate.html">read more</a>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-md-4 col-lg-4">
                                    <div class="schedule-box maxwidth500 mb-30" data-bg-img="images/pattern/p6.png">
                                        <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="images/events/1.jpg">
                                        </div>
                                        <div class="schedule-details clearfix p-15 pt-10">
                                        <div class="text-center pull-left flip bg-theme-color-sky p-10 pt-5 pb-5 mr-10">
                                            <ul>
                                            <li class="font-19 text-white font-weight-600 border-bottom ">28</li>
                                            <li class="font-12 text-white text-uppercase">Feb</li>
                                            </ul>
                                        </div>
                                        <h4 class="title mt-0"><a href="#">Business Conferences 2017</a></h4>
                                        <ul class="list-inline font-11 text-black">
                                            <li><i class="fa fa-calendar mr-5"></i> Sundat at 9:30PM</li>
                                            <li><i class="fa fa-map-marker mr-5"></i> 89 Newyork City.</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <p class="mt-10">Lorem ipsum dolor sit amet elit. Cum veritatis sequi nulla nihil, dolor voluptatum nemo adipisci eligendi! Sed nisi perferendis, totam harum dicta.</p>
                                        <div class="mt-10">
                                            <a href="#" class="btn btn-default btn-theme-color-sky mt-10 font-16 btn-sm" href="page-donate.html">read more</a>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-4">
                                    <div class="schedule-box maxwidth500 mb-30" data-bg-img="images/pattern/p6.png">
                                        <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="images/events/1.jpg">
                                        </div>
                                        <div class="schedule-details clearfix p-15 pt-10">
                                        <div class="text-center pull-left flip bg-theme-color-sky p-10 pt-5 pb-5 mr-10">
                                            <ul>
                                            <li class="font-19 text-white font-weight-600 border-bottom ">28</li>
                                            <li class="font-12 text-white text-uppercase">Feb</li>
                                            </ul>
                                        </div>
                                        <h4 class="title mt-0"><a href="#">Business Conferences 2017</a></h4>
                                        <ul class="list-inline font-11 text-black">
                                            <li><i class="fa fa-calendar mr-5"></i> Sundat at 9:30PM</li>
                                            <li><i class="fa fa-map-marker mr-5"></i> 89 Newyork City.</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <p class="mt-10">Lorem ipsum dolor sit amet elit. Cum veritatis sequi nulla nihil, dolor voluptatum nemo adipisci eligendi! Sed nisi perferendis, totam harum dicta.</p>
                                        <div class="mt-10">
                                            <a href="#" class="btn btn-default btn-theme-color-sky mt-10 font-16 btn-sm" href="page-donate.html">read more</a>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-4">
                                    <div class="schedule-box maxwidth500 mb-30" data-bg-img="images/pattern/p6.png">
                                        <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="images/events/1.jpg">
                                        </div>
                                        <div class="schedule-details clearfix p-15 pt-10">
                                        <div class="text-center pull-left flip bg-theme-color-sky p-10 pt-5 pb-5 mr-10">
                                            <ul>
                                            <li class="font-19 text-white font-weight-600 border-bottom ">28</li>
                                            <li class="font-12 text-white text-uppercase">Feb</li>
                                            </ul>
                                        </div>
                                        <h4 class="title mt-0"><a href="#">Business Conferences 2017</a></h4>
                                        <ul class="list-inline font-11 text-black">
                                            <li><i class="fa fa-calendar mr-5"></i> Sundat at 9:30PM</li>
                                            <li><i class="fa fa-map-marker mr-5"></i> 89 Newyork City.</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <p class="mt-10">Lorem ipsum dolor sit amet elit. Cum veritatis sequi nulla nihil, dolor voluptatum nemo adipisci eligendi! Sed nisi perferendis, totam harum dicta.</p>
                                        <div class="mt-10">
                                            <a href="#" class="btn btn-default btn-theme-color-sky mt-10 font-16 btn-sm" href="page-donate.html">read more</a>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </section>
                       

                       
                       
                       

                       
                   
                    </div>                
                </div>
            </div>
        </div>
        <div> 
            <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
        </div>
        </section>
           

        </div>
        <!--/ ends main content -->

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>