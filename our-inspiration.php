<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">Our Inspiration</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="#">About</a></li>
                                <li class="active text-gray-silver">Our Inspiration</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: About -->
        <section>
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12 pricing-table">
                        <!-- <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join</span>Us</h2> -->
                       
                        <p>A friend of ours was walking down a deserted Mexican beach at sunset. As he walked along, he began to see another man in the distance. As he grew nearer, he noticed that the local native kept leaning down, picking something up, and throwing it out into the water. Time and again, he kept hurling something out into the ocean.</p>
                        
                        <p>As our friend approached even closer, he noticed that the man was picking up starfish that had washed up on the beach and, one at a time, was throwing them back into the water.</p>

                        <p>Our friend was puzzled. He approached the man and said, 'Good evening, friend. I was wondering what you are doing?'</p>

                        <p>'I'm throwing these starfish back into the ocean. You see, it is low tide right now, and all of these starfish have washed up onto the shore. If I don't throw them back into the sea, they'll die from lack of oxygen.'</p>

                        <p>'I understand,' my friend replied, 'but there must be thousands of starfish on this beach! You can't possibly get to all of them. There are simply too many! And don't you realize this is probably happening on hundreds of beaches all up and down this coast? Can't you see that you can't possibly make a difference?'</p>

                        <p>The local native smiled, bent down, and picked up yet another starfish and, as he threw it back into the sea replied, "Made a difference to that one!!!"</p>

                        <p>NO MATTER WHERE YOU ARE IN THE WORLD, IF YOU HAVE DECIDED TO DO SOMETHING DEEP FROM YOUR HEART, YOU CAN DO IT. IT IS THE THOUGHT THAT MATTERS NOT WHERE YOU ARE OR WHERE THE PERSON IS.</p>


                       
                   
                    </div>                
                </div>
            </div>
        </div>
        <div> 
            <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
        </div>
        </section>
           

        </div>
        <!--/ ends main content -->

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>