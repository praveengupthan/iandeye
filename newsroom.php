<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">News Room</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li> 
                                <li class="active text-gray-silver">News Room</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: container  -->
        <section>
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col 12-->
                    <div class="col-lg-12">
                        <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">News</span> Room</h2>

                        <div class="section-content">
                        <div class="row mb-30">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/news/1.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade red"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/news/1.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/news/2.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade yellow"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/news/2.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/news/3.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade green"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/news/3.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-30">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/news/4.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade green"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/news/4.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/news/5.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade blue"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/news.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/news/6.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade yellow"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/news/6.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/news/7.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade blue"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/news/7.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/news/8.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade blue"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/news/8.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/news/9.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade blue"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/news/9.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="gallery-block">
                                    <div class="gallery-thumb">
                                        <img alt="project" src="images/news/10.jpg" class="img-fullwidth">
                                    </div>
                                    <div class="overlay-shade blue"></div>
                                    <div class="icons-holder">
                                        <div class="icons-holder-inner">
                                            <div class="gallery-icon">
                                                <a href="images/news/10.jpg" data-lightbox-gallery="gallery"><i
                                                        class="pe-7s-science"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                        

                    </div>
                    <!--/ col 12-->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
       
            <div> 
                <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
            </div>
        </section>
           

        </div>
        <!--/ ends main content -->

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>