<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">USA</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li> 
                                <li class="active text-gray-silver">USA</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: About -->
        <section>
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join</span>Us</h2> -->

                        <p>Our US Chapter started with Chandrareddy Manubotu, who joined us in the very beginning, when we were collecting funds to donate books to Home for Orphan Students, Cherlapally. Through him his friend, Sitaram Jakka, too joined. Sitaram joined his friend, Gowtam. Later Divya joined when we were collecting funds for the kidney operation of Raju. She came to know us through TeluguPeople.com.</p>

                        <p>After getting engaged to Chandrareddy, Yasoda joined. Suresh Ediga joined us by reading my message to him at Ryze Network. Nachaki alias Nallan Chakravartula Kiran joined us through my mail to him in one of the telugu yahoo groups. Prasad Charasala garu joined us through Veeven. Veeven came to know about our group when we conducted a meeting with TeluguBrains members. Karthik Jalamangala joined us after going through a message in Orkut Community.</p>

                        <p>Thottempudi Suneel joined us through TeluguPeople.com. He relocated to US from Bangalore. Sravan Kumar Patnaik, WWHY Co-ordinator, is on official trip to US. He is our Hyderabad member.</p>

                        <h4>Members</h4>
                        <ul class="table-list">
                            <li><i class="fa fa-check"></i>Chandrareddy Manubotu, cmanubot@cisco.com, 408-316-5994 (Cell) </li>
                            <li><i class="fa fa-check"></i>Yasoda Dantla, yasodha.d@gmail.com </li>
                            <li><i class="fa fa-check"></i>Sitaram Jakka, sitaram.j@gmail.com </li>
                            <li><i class="fa fa-check"></i>Gowtham </li>
                            <li><i class="fa fa-check"></i>Divya </li>
                            <li><i class="fa fa-check"></i>Nallan Chakravartula Kiran, email4groups@gmail.com </li>
                            <li><i class="fa fa-check"></i>TS Kumar Reddy, tskumarreddy@yahoo.com </li>
                            <li><i class="fa fa-check"></i>Srinivas Dhanraj, srinivasdhanraj@yahoo.com </li>
                            <li><i class="fa fa-check"></i>Suresh Ediga, suresh.e@gmail.com, 908-917-2846 (Cell) </li>
                            <li><i class="fa fa-check"></i> Karthik Jalamangala, karthik.jalamangala@gmail.com</li>
                            <li><i class="fa fa-check"></i>Prasad Charasala, charasala@gmail.com, 301-693-0429 (Cell), 202-366-1294 (Work) </li>
                            <li><i cla
                            ss="fa fa-check"></i>Ramanatha Reddy, reddy.ramanadha@gmail.com </li>
                            <li><i class="fa fa-check"></i>Dr. Ismail Penukonda, drchinthu@gmail.com </li>
                            <li><i class="fa fa-check"></i>Thottempudi Suneel, suneel.thottempudi@gmail.com </li>
                            <li><i class="fa fa-check"></i>Sravan Kumar Patnaik, sravan24@yahoo.com </li>
                            <li><i class="fa fa-check"></i>Venkat Ganne, venkatganne@yahoo.com</li>
                            <li><i class="fa fa-check"></i>Kavitha Nistala</li>
                            <li><i class="fa fa-check"></i>Sai Deepika </li>
                            <li><i class="fa fa-check"></i>Suresh Gadamsetti, s_gadamsetti@yahoo.co.in </li>
                            <li><i class="fa fa-check"></i>Uday Bhaskar Pratti, ubsmart@yahoo.com, 425-802-8567 (Cell), 425-818-0881 (Home) </li>                            
                        </ul>  

                        <h4>Activities</h4>
                        <p>US members send their contributions to us. Apart from that, they regularly follow the mails and offer their suggestions. They take any responsibility that they can shoulder. They even ring to the people with whom we deal because of the cases that we take up.</p>

                        <p>They meet over a Skype Calll every month usually on First Sunady of every month, 11 Am EST </p>


                        
          


                    </div>                
                </div>
            </div>
        </div>
        <div> 
            <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
        </div>
        </section>
           

        </div>
        <!--/ ends main content -->

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>