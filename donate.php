<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">Donate Now</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li> 
                                <li class="active text-gray-silver">Donate Now</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: container  -->
        <section>
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col 12-->
                    <div class="col-lg-12">
                        <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Donate</span> Now</h2>

                        <p class="text-center">As we reach out to more and more people and as the time pass by, the requirements are huge and we are falling short of money though we found that the cases are genuine after thorough verification.</p>

                        <p class="text-center"> TMAD IS IN NEED OF YOUR SUPPORT. DO COME FORWARD TO GENEROUSLY CONTRIBUTE FOR GENUINE CAUSES</p>

                        <p class="text-center">IT IS NOT CHARITY, IT IS NOT SYMPATHY, IT IS OUR RESPONSIBILITY. COME FORWARD!! JOIN HANDS WITH US TO MAKE A DIFFERENCE!</p>

                        <p class="text-center">One of the characteristics that make us unique as an organisation is our limitless vision and boundless compassion. We have undertaken every project which we felt can make a difference in the life of an individual or a community; right from tree plantation drives and blood donation camps to supporting individuals in their need of financial aid.</p>

                        <p class="text-center">Every quarter, time and again, we, the TMADians, prove that every drop in the ocean counts. Our passion makes us believe that we can make a difference in this world. We come across so many inspiring people, people who motivated us to do better, to serve more, and to reach out to more people in need. This fuels our desire to serve more.</p>

                        <p class="text-center">FOREIGN TRANSACTIONS ARE NOT ALLOWED AS WE ARE NOT ELIGIBLE.SO IF ANY ARE INTERESTED PLEASE DROP A MAIL TO FINANCE@TMAD.ORG FURTHER WE WILL GUIDE YOU</p>

                        <p class="text-center">DONORS WITH AN ACCOUNT IN INDIA MAY TRANSFER THEIR CONTRIBUTIONS TO THE FOLLOWING ACCOUNT:

Our banker</p>

                        <p class="text-center">Beneficiary/Account Name: TO MAKE A DIFFERENCE (TMAD) </br>

                            Account Number: 000801210382  </br>

                            Account Type: Current  </br>

                            Branch: CIBD, Hyderabad  </br>

                            NEFT/IFSC/RTGS Code: ICIC0000008  </br>
                        </p>

                        <h4 CLASS="text-center"></h4>

                        <p class="text-center">Are you looking for details on how we have spent donors money all these years?? </br>

                        How much money spent on each case??  </br>
                          
                        </p> 
                    </div>
                    <!--/ col 12-->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
       
            <div> 
                <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
            </div>
        </section>
           

        </div>
        <!--/ ends main content -->

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>