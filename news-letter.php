<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">News Letter</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="#">About</a></li>
                                <li class="active text-gray-silver">News letter</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: About -->
        <section>
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12 pricing-table">
                        <!-- <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join</span>Us</h2> -->

                        <div class="pb-3">
                            <h5>"NO MATTER WHERE YOU ARE IN THE WORLD, IF YOU HAVE DECIDED TO DO SOMETHING DEEP FROM YOUR HEART, YOU CAN DO IT".</h5>
                            <p>In line with this commitment, we now present you, our Quarterly Newsletters. </p>
                        </div>

                        <h4>2016 Newsletter Editions</h4>
                        <h5>Q3 2016 Edition - Highlights</h5>
                        <ul class="table-list">
                            <li><i class="fa fa-check"></i>  5 Educational cases</li>
                            <li><i class="fa fa-check"></i> 5 Medical cases</li>
                            <li><i class="fa fa-check"></i> 4 Campaigns (Blood donation, Eco Friendly Ganesha, School kits distribution, Events at School on Independence day)</li>
                        </ul>  

                        <h5>Q2 2016 Edition - Highlights</h5>
                        <ul class="table-list">
                            <li><i class="fa fa-check"></i> 4 Educational cases</li>
                            <li><i class="fa fa-check"></i> 4 Medical cases</li>
                            <li><i class="fa fa-check"></i> Livelihood support for Farmer @ Mahbubnagar.</li>
                            <li><i class="fa fa-check"></i> Slipper Distribution at Government School, Anantpur</li>
                        </ul>  

                       
                       
                       

                       
                   
                    </div>                
                </div>
            </div>
        </div>
        <div> 
            <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
        </div>
        </section>
           

        </div>
        <!--/ ends main content -->

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>