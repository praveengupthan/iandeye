<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">Bengaluru</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li> 
                                <li class="active text-gray-silver">Bengaluru</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: About -->
        <section>
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join</span>Us</h2> -->

                        <p>Mahidhar, Rajasekhar and Subrahmanyam Mula are the first ones to join TMAD group from Bangalore. Mahidhar is Prasanthi's classmate. Rajasekhar and Subrahmanyam Mula are friends through TeluguPeople.com. Other members from Bangalore at that time are also friends of Prasanthi.</p>  

                        <p>The first monthly meeting of Bangalore was conducted on Sunday 5th of February 2006 at Rajasekhar's room, BTM Layout. From them we have been conducting monthly meetings.</p>                     

                        <h4>Members:</h4>
                        <ul class="table-list">
                            <li><i class="fa fa-check"></i>Pavan Kumar Pyda - Vice President </li>
                            <li><i class="fa fa-check"></i>Mobile : +91 9986329966 </li>
                            <li><i class="fa fa-check"></i>Mail : pavan.pyda@tmad.org </li>
                        </ul>                         
                        <ul class="table-list">
                            <li><i class="fa fa-check"></i>Anilkumar BVN - Joint Secretary </li>
                            <li><i class="fa fa-check"></i>Mobile : +91 9880994182 </li>
                            <li><i class="fa fa-check"></i>Mail : anilkumarbvn@tmad.org</li> 
                        </ul>  

                        <h4>Contact Numbers:</h4>
                        <ul class="table-list">
                            <li><i class="fa fa-check"></i>Kasyap Palivela - 9494466189/9396533666 (Executive board member) </li>
                        </ul>  

                        <h4>Events:</h4>
                        <ul class="table-list">
                            <li><i class="fa fa-check"></i>First anniversary is celebrated by Kiran Nikam and Pavan Pyda. They donated blood in Red Cross Hospital. </li>
                            <li><i class="fa fa-check"></i>For Second anniversary, our members met at Cubbon Park. </li>
                            <li><i class="fa fa-check"></i>For Third Anniversary, our members met at Lalbagh.</li>
                        </ul>                          

                    </div>                
                </div>
            </div>
        </div>
        <div> 
            <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
        </div>
        </section>
           

        </div>
        <!--/ ends main content -->

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>