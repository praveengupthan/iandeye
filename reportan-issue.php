<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">Report an Issue</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="#">About</a></li>
                                <li class="active text-gray-silver">Report an Issue</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: About -->
        <section>
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12 pricing-table">
                        <!-- <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join</span>Us</h2> -->
                        <p>Please supply a short title that describes your issue, and in the description box provide as much detail as possible to enable us to try and quickly resolve your problem.</p>

                        <!-- Divider: Contact -->
                        <section class="divider">
                        <div class="container pt-50 pb-70">
                            <div class="row pt-10">                            
                            <div class="col-md-12">
                                <h4 class="mt-0 mb-30 line-bottom">Create an Issue</h4>
                                <!-- Contact Form -->
                                <form id="contact_form" name="contact_form" class="" action="" method="post">

                                <div class="row">
                                    <div class="col-sm-6">
                                    <div class="form-group mb-30">
                                        <input id="form_name" name="form_name" class="form-control" type="text" placeholder="Enter Name" required="">
                                    </div>
                                    </div>
                                    <div class="col-sm-6">
                                    <div class="form-group mb-30">
                                        <input id="form_email" name="form_email" class="form-control required email" type="email" placeholder="Enter Email">
                                    </div>
                                    </div>
                                </div>                
                                <div class="row">
                                    <div class="col-sm-6">
                                    <div class="form-group mb-30">
                                        <input id="form_subject" name="form_subject" class="form-control required" type="text" placeholder="Website Address">
                                    </div>
                                    </div>
                                    <div class="col-sm-6">
                                    <div class="form-group mb-30">
                                        <input id="form_phone" name="form_phone" class="form-control" type="text" placeholder="Enter Phone">
                                    </div>
                                    </div>
                                </div>

                                <div class="form-group mb-30">
                                    <textarea id="form_message" name="form_message" class="form-control required" rows="7" placeholder="Description"></textarea>
                                </div>
                                <div class="form-group">
                                    <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="" />
                                    <button type="submit" class="btn btn-dark btn-theme-color-blue btn-flat mr-5" data-loading-text="Please wait...">Send your message</button>
                                   
                                </div>
                                </form>
                            </div>
                            </div>
                            
                        </div>
                        </section>

                       
                       

                       
                   
                    </div>                
                </div>
            </div>
        </div>
        <div> 
            <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
        </div>
        </section>
           

        </div>
        <!--/ ends main content -->

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>