<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">Hyderabad</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li> 
                                <li class="active text-gray-silver">Hyderabad</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: About -->
        <section>
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join</span>Us</h2> -->

                        <p>Hyderabad is the main Chapter. Most of the activities are executed here. Activities go on in each subgroup: Education, Health and Likemindedgroups.</p>                       

                        <h4>Monthly Meeting Details</h4>
                        <ul class="table-list">
                            <li><i class="fa fa-check"></i>Date: First sunday of every month </li>
                            <li><i class="fa fa-check"></i>Timings: 10 am to 12:30 pm. </li>
                            <li><i class="fa fa-check"></i>Venue: Aarogya Dhatri Ayurvedic Clinic, Flat No: 5, Sarada Apartments, Opp. Ameerpet Dental Hospital, Lane opp. Swagath Hotel and beside Sri Krishna Sweets, Ameerpet. Phone: 040- 39104638 </li>                                                   
                        </ul>  

                        <h4>Members</h4>
                        <ul class="table-list">
                            <li><i class="fa fa-check"></i>President: Harinath Reddy </li>
                            <li><i class="fa fa-check"></i>Mobile: +91-9391920444 </li>
                            <li><i class="fa fa-check"></i>Mail: harinath@tmad.org </li>  
                            <li><i class="fa fa-check"></i>Secretary: U L Prasanthi </li>  
                            <li><i class="fa fa-check"></i>Mobile: +91-9885026676 </li>  
                            <li><i class="fa fa-check"></i>MMail: Prasanthi@tmad.org </li>                                                               
                        </ul>  

                        <h4>Contact Numbers:</h4>
                        <ul class="table-list">
                            <li><i class="fa fa-check"></i>Kasyap Palivela - 9494466189/9396533666 (Executive board member) </li>
                        </ul>  

                        <h4>Activities:</h4>
                        <ul class="table-list">
                            <li><i class="fa fa-check"></i>Education: Visit to Mehadipatnam School on every saturday </li>
                        </ul>  

                        <h4>Events:</h4>
                        <ul class="table-list">
                            <li><i class="fa fa-check"></i>Trip to Warangal on Mid of October'2010 </li>
                        </ul>

                    </div>                
                </div>
            </div>
        </div>
        <div> 
            <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
        </div>
        </section>
           

        </div>
        <!--/ ends main content -->

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>