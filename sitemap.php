<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">Site Map</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li> 
                                <li class="active text-gray-silver">Sitemap</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: container  -->
        <section>
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col 12-->
                    <div class="col-lg-12">
                        <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Main</span> menu</h2>

                        <ul class="table-list">
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Home</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">About us</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Events</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Projects</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Vijay Santhosh, 5 yrs, Speech Therapy, Gachibowli, Hyderabad, TS</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">2019 New Year Celebration: Medicines distribution @ Kumidini Hospice, Hyderabad, TG</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Medical Help - Bhavani</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Baby Alekhya, 7 Yrs, Brain Surgery, Nizamabad, TG</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Phanindra, 15 Yrs, Neuro treatment, Hyderabad, TG</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Lakshmi, 33 Yrs, Leg Injury, Sanjaynagar, Bangalore, KA</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Gopika Kumar, 4.5 Yrs, Heart Operation, Tiruchi, TN</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Babu, 30 Yrs, Kidney failure, Martur, Prakasam, AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Diabetes Outreach Program</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Ramakrishna, 33 Yrs, Kidney failure, Kadiri, Anantapur, AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Krishna Rao, 54 Yrs, Lung and Liver Problem, Vizag, AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Ramana, 52 yrs, Neuro problem, Guntur, AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Prasad, 52 yrs, Chest Pain, Kavali, Nellore, AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Vedamani, 65Yrs, Neuroand Cardio problem, Nagarjuna Sagar, Guntur Dt, AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Doraswamy, 35 Yrs, Vision loss, Baireddipalle, Chittoor, AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Monica, 12 Yrs, Dengue Fever, Adugodi, Bengaluru, KA</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Need Support for Tanish for BMT</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Lingachary, 30 Yrs, Decreased Vison, Nutankal, Nalgonda, AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Manohar, 17 Yrs, Thalassemia (Blood Transfusion), Hindupur, Anantapur , AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Akash, 12 Yrs, Thalassemia, Gavipuram, Bengaluru, KA</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Yuvaraj, 25 Yrs, Spinal Cord injury, Nellore, AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Ganapathi, 58 Yrs, Neuro Problem, Vijayawada, AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Pragalya, 2 Yrs, Kidney Problem, Aruppukkottai, Virudhunagar, TN</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">B/o Sreelatha, 28 days, Heart Problem, Annasandrapalaya, Bangalore, Karnataka</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Badhulla Bee Shaik, 9 yrs, Polio, Buchireddy palem, Nellore Dist, AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Vishnu, 28 yrs, Post Kidney Transplantation support</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Shejad, 17 Yrs, Electric Burn Injury, Tarikund, Jagatsinghpur, Odisha</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Ramana, Brain Tumor, Hyderabad, AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Tulsi Merekar, suffering from eye problem, Surat, Gujarat.</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Need support for patients from BKothaPalli (10 Km from Kadiri, Anantapur), AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Lavisha, 2 Yrs, suffering from Epilepsy, Surat, Gujarat</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Sravanthi, Throat operation, Ulavapadu, Prakasam Dist, AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Mounika, 23 yrs, Scoliosis Problem, Sangareddy, Medak, AP</a>
                            </li>
                            <li>
                                <i class="fa fa-check"></i> 
                                <a href="javascript:void(0)">Ram Teja, 20 years, suffering from Brain Tumour</a>
                            </li>                           
                        </ul>  

                    </div>
                    <!--/ col 12-->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
       
            <div> 
                <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
            </div>
        </section>
           

        </div>
        <!--/ ends main content -->

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>