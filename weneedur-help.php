<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />

    <!-- Page Title -->
    <title>i and Eye</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <?php include 'source.php' ?>
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        <div id="preloader">
            <div id="spinner">
                <div class="preloader-dot-loading">
                    <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                </div>
            </div>
            <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
        </div>

       <?php include 'header.php' ?>

        <!-- Start main-content -->
        <div class="main-content subpage">

        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg3.jpg">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">We Need your help</h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="#">About</a></li>
                                <li class="active text-gray-silver">We Need your help</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: About -->
        <section>
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12 pricing-table">
                        <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Need</span>Your Help</h2>
                        <h4 class="">THANK YOU FOR HAVING THOUGHT TO HELP THE SOCIETY</h4>
                        <p>Here we listed few cases where we need continuous help from the kind hearted people. The help varies from volunteering to monetary.</p>
                        
                        <h2 class="line-bottom-center mt-0">Medical</h2>                                               

                        <ul class="table-list">
                            <li><i class="fa fa-check"></i> Ramakrishna, 55 years man is suffering from Kidney problem. He requires around Rs. 15,930 every month for the medicines/treatment.Click Here for more details.</li>

                            <li><i class="fa fa-check"></i> Babu, 30 years man is suffering with Kidney failure.He requires around Rs. 6,563 every month for the Medicines/treatment.Click Here for more details.</li>

                            <li><i class="fa fa-check"></i> Phanindra, 15 Yrs boy recovering from coma. He is looking for support for the remaining two sittings of treatment. Each sitting costs them Rs.2.5L. Click Here for more details.</li>

                            <li><i class="fa fa-check"></i> Lakshmi, 33 Yrs women is a Thalassemia patient from childhood. She had undergone leg surgery due to bone infection and looking for support. Click Here for more details. </li>

                            <li><i class="fa fa-check"></i> Ramana, 52 yrs man looking support to buy monthly medicines. He is bed ridden from past 8 years with no income. Click Here for more details.</li>

                            <li><i class="fa fa-check"></i> Prasad, 52 yrs man looking support to buy monthly medicines. He & his brother living together from their child. No family. Click Here for more details.</li>
                        </ul>  

                         <h2 class="line-bottom-center mt-0">EDUCATIONAL</h2> 
                        
                        <ul class="table-list">
                            <li><i class="fa fa-check"></i> TMAD has been distributing School kits every year  for students in Govt schools, for this academic year 2018-19 we are planning to distribute kits till now we have distributed 4376 kits. Each kit cost is Rs 100 and each kit consists of 5 notebooks,1 slate,1 pen,1 pencil and 1 eraser.Click Here for more details.</li>

                            <li><i class="fa fa-check"></i> TMAD Volunteers taking special classes at Vivek Nagar Govt High School. We need more volunteers for teaching. Click Here for more details.contact @ infoblr@tmad.org </li>
                        </ul>  

                        <p>Please support the needy by sending your contributions to the below account from your India account only. Even Rs 100 also Makes A Difference to the needy.</p> 

                        <p>FOREIGN TRANSACTIONS ARE NOT ALLOWED AS WE ARE NOT ELIGIBLE.SO IF ANY ARE INTERESTED PLEASE DROP A MAIL TO FINANCE@TMAD.ORGFURTHER WE WILL GUIDE YOU</p>

                        <h4 class=""> DONORS WITH AN ACCOUNT IN INDIA MAY TRANSFER THEIR CONTRIBUTIONS TO THE BELOW ACCOUNT:</h4>

                        <table>
                            <tr>
                                <td>Account Name: </td>
                                <td>TO MAKE A DIFFERENCE (TMAD)</td>
                            </tr>
                            <tr>
                                <td>Account Number:</td>
                                <td>000801210382</td>
                            </tr>
                            <tr>
                                <td>Account Type:</td>
                                <td>Current</td>
                            </tr>
                            <tr>
                                <td>Branch:</td>
                                <td>CIBD, Hyderabad</td>
                            </tr>
                            <tr>
                                <td>NEFT/IFSC/RTGS Code:</td>
                                <td>ICIC0000008</td>
                            </tr>                            
                        </table>

                        <h5>Note</h5>

                        <ul>
                            <li>50% of your donations to TMAD are exempted from income tax under 80G.</li>
                            <li>Request to send a mail to finance@tmad.org with the details of your transaction and the name details to issue the receipt.</li>
                            <li>The receipts will be issued within a month of receiving the contribution.</li>
                            <li> Any queries related to donation can be sent to this ID:  finance@tmad.org</li>
                        </ul>

                       
                       
                    </div>                
                </div>
            </div>
        </div>
        <div> 
            <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
        </div>
        </section>
           

        </div>
        <!--/ ends main content -->

       <?php include 'footer.php' ?>
    </div>
    <!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
</body>

</html>